
" Plug Config
call plug#begin('~/.nvim/plugged')

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'sbdchd/neoformat'

call plug#end()

g:airline_theme='zenburn'

syntax enable
